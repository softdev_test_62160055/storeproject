/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumokung.storeproject.poc;

import database.Database;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author ASUS
 */
public class TestInsertProduct {

    public static void main(String[] args) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            Product product = new Product(-1,"Americanno",40);
            String insertProduct = "INSERT INTO PRODUCT(NAME,PRICE)"
                    + "VALUES ('" + product.getName() 
                    + "',"+ product.getPrice() + ");";
            stmt.executeUpdate(insertProduct);

            stmt.close();
            conn.commit();
            db.close();
        }catch (SQLException ex) {
            Logger.getLogger(TestInsertProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
