/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumokung.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author ASUS
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"Mocca",30);
        Product p2 = new Product(2,"Latte",40);
        User seller = new User("Sumo","pasword","0992595558");
        Customer customer = new Customer("Phai","0888888888");
        Receipt receipt = new Receipt(seller,customer);
        
        
        receipt.addReceiptDetail(p1,1);
        receipt.addReceiptDetail(p2,3);
        
        System.out.println(receipt);
        receipt.addReceiptDetail(p2,3);
        System.out.println(receipt);
        
    }
}
