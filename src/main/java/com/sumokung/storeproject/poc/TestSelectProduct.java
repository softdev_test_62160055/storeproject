/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumokung.storeproject.poc;

import database.Database;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author ASUS
 */
public class TestSelectProduct {
    public static void main(String[] args) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM PRODUCT;" );
            
            while(rs.next()){
                int id = rs.getInt("id");
                String  name = rs.getString("name");
                double price = rs.getDouble("price");
                Product product = new Product(id,name,price);
                System.out.println(product);
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
