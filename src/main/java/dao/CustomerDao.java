/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author ASUS
 */
public class CustomerDao implements DaoInterface<Customer>{

    @Override
    public int add(Customer object) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            int id = -1;
            String insertCustomer = "INSERT INTO CUSTOMER(NAME,TEL)"
                    + "VALUES ('" + object.getName() 
                    + "','" + object.getTel() + "');";
            stmt.executeUpdate(insertCustomer);
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
            
            stmt.close();
            conn.commit();
            db.close();
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM CUSTOMER;" );
            
            while(rs.next()){
                int id = rs.getInt("id");
                String  name = rs.getString("name");
                String  tel = rs.getString("tel");
                Customer cus = new Customer(id,name,tel);
                list.add(cus);
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public Customer get(int id) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM CUSTOMER WHERE ID = "
                                             + id +";");
            
            while(rs.next()){
                id = rs.getInt("id");
                String  name = rs.getString("name");
                String  tel = rs.getString("tel");
                Customer cus = new Customer(id,name,tel);
                return cus;
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        int row =0;

        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            
            String deleteCustomer = "DELETE from CUSTOMER where ID = '"
                                   + id +"';";
            row = stmt.executeUpdate(deleteCustomer);
            conn.commit();
            stmt.close();
            db.close();
        } catch (SQLException ex) { 
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return row;
    }

    @Override
    public int update(Customer object) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        int row =0;

        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("UPDATE CUSTOMER set TEL ="+object.getTel()
                                    +" where ID="+object.getId()+"");
            conn.commit();
            stmt.close();
            db.close();
            
        } catch (SQLException ex) { 
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return row;
    }
    
    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1,"Ten","07777777"));
        System.out.println("id : " + id);
        System.out.println(dao.get(id));
        Customer lastCus = dao.get(id);
        lastCus.setTel("09999999");
        int row = dao.update(lastCus);
        System.out.println(row);
        Customer updateCus = dao.get(id);
        System.out.println("update customer:" + updateCus);
        dao.delete(id);
        Customer deleteCus = dao.get(id);
        System.out.println("delete customer: " + deleteCus);
    }
}
