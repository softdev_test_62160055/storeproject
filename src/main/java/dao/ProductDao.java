/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.sumokung.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author ASUS
 */
public class ProductDao implements DaoInterface<Product>{

    @Override
    public int add(Product product) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            int id = -1;
            String insertProduct = "INSERT INTO PRODUCT(NAME,PRICE)"
                    + "VALUES ('" + product.getName() 
                    + "',"+ product.getPrice() + ");";
            stmt.executeUpdate(insertProduct);
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
            
            stmt.close();
            conn.commit();
            db.close();
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;

    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM PRODUCT;" );
            
            while(rs.next()){
                int id = rs.getInt("id");
                String  name = rs.getString("name");
                double price = rs.getDouble("price");
                Product product = new Product(id,name,price);
                list.add(product);
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public Product get(int id) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM PRODUCT WHERE ID ="
                                             + id +";" );
            
            while(rs.next()){
                id = rs.getInt("id");
                String  name = rs.getString("name");
                double price = rs.getDouble("price");
                Product product = new Product(id,name,price);
                return product;
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    

    @Override
    public int delete(int id) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        int row =0;

        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            
            String deleteProduct = "DELETE from PRODUCT where ID = '"
                                   + id +"';";
            row = stmt.executeUpdate(deleteProduct);
            conn.commit();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return row;
    }

    @Override
    public int update(Product product) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        int row =0;

        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("UPDATE PRODUCT set PRICE ="+product.getPrice()
                                    +" where ID="+product.getId()+"");
            conn.commit();
            stmt.close();
            db.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return row;
    }
    
    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(2));
        int id = dao.add(new Product(-1,"coffee",50));
        System.out.println("id : " + id);
        Product lastProduct = dao.get(id);
        lastProduct.setPrice(100);
        int row = dao.update(lastProduct);
        System.out.println(row);
        Product updateProduct = dao.get(id);
        System.out.println("update product:" + updateProduct);
        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("delete product: " + deleteProduct);
        
    }
    
}
