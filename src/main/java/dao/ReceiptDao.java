/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.sumokung.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author ASUS
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        Statement stmtdetail = null;
        conn = db.getConnection();

        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            int id = -1;
            String insertReceipt = "INSERT INTO RECEIPT (CUSTOMER_ID,USER_ID,TOTAL) "
                    + "VALUES (" + object.getCustomer().getId() + ","
                    + object.getSeller().getId() + ","
                    + object.getTotal() + ");";
            stmt.executeUpdate(insertReceipt);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }

            for (ReceiptDetail r : object.getReceiptDetail()) {
                stmtdetail = conn.createStatement();
                String sqla = "INSERT INTO RECEIPT_DETAIl (RECEIPT_ID,PRODUCT_ID,PRICE,AMOUNT) "
                        + "VALUES (" + r.getReceipt().getId() + ","
                        + r.getProduct().getId() + ","
                        + r.getPrice() + ","
                        + r.getAmount() + ");";
                stmtdetail.executeUpdate(sqla);
                ResultSet resultdetail = stmtdetail.getGeneratedKeys();
                if (resultdetail.next()) {
                    id = resultdetail.getInt(1);
                }
            }

            stmt.close();
            conn.commit();
            db.close();
            return id;
        } catch (SQLException ex) {
            System.out.println("Error to created receipt");
        }
        return -1;

    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);

            String sql = "SELECT r.id as id,\n"
                    + "       r.created as created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + "  FROM receipt r,customer c, user u\n"
                    + "  WHERE r.customer_id = c.id AND r.user_id = u.id;";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("created"));
                int customerId = rs.getInt("customer_id");
                String customerName = rs.getString("customer_name");
                String customerTel = rs.getString("customer_tel");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String userTel = rs.getString("user_tel");
                double total = rs.getDouble("total");
                Receipt receipt = new Receipt(id, created, new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));
                receipt.setTotal(total);
                list.add(receipt);
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt");
        } catch (ParseException ex) {
            System.out.println("Error: Date passing");
        }
        return list;
    }

    @Override
    public Receipt get(int id) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            String sql = "SELECT r.id as id,\n"
                    + "       r.created as created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + "  FROM receipt r,customer c, user u\n"
                    + "  WHERE r.id = ? AND r.customer_id = c.id AND r.user_id = u.id"
                    + "  ORDER BY created DESC;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int rid = rs.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("created"));
                int customerId = rs.getInt("customer_id");
                String customerName = rs.getString("customer_name");
                String customerTel = rs.getString("customer_tel");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String userTel = rs.getString("user_tel");
                double total = rs.getDouble("total");
                Receipt receipt = new Receipt(rid, created, new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));
                receipt.setTotal(total);
                getReceiptDetail(conn, id, receipt);

                return receipt;
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id =" + id + ": "
                               + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date passing");
        }
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n"
                + "       receipt_id,\n"
                + "       product_id,\n"
                + "       name,\n"
                + "       rd.price as price,\n"
                + "       amount\n"
                + "  FROM receipt_detail rd,product p\n"
                + "  WHERE receipt_id = ? AND rd.product_id = p.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet rsDetail = stmtDetail.executeQuery();
        while(rsDetail.next()){
            int detailId = rsDetail.getInt("id");
            int productId = rsDetail.getInt("product_id");
            String productName = rsDetail.getString("name");
            double price = rsDetail.getDouble("price");
            int amount = rsDetail.getInt("amount");
            Product product = new Product(productId, productName, price);
            receipt.addReceiptDetail(detailId,product, amount);
        }
    }

    @Override
    public int delete(int id) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        int row = 0;

        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            String deleteReceipt = "DELETE from RECEIPT where ID = '"
                    + id + "';";
            row = stmt.executeUpdate(deleteReceipt);
            conn.commit();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id" + id);
        }
        return row;
    }

    @Override
    public int update(Receipt product) {
//        java.sql.Connection conn = null;
//        Database db = Database.gatInstance();
//        Statement stmt = null;
//        conn = db.getConnection();
//        int row = 0;
//
//        try {
//            db.getConnection();
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//            row = stmt.executeUpdate("UPDATE PRODUCT set PRICE =" + product.getPrice()
//                    + " where ID=" + product.getId() + "");
//            conn.commit();
//            stmt.close();
//            db.close();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(2, "Mocca", 40);
        Product p2 = new Product(9, "Americanno", 90);
        User seller = new User(1, "Sumo", "0992595558");
        Customer customer = new Customer(1, "Book", "087654344578");
        Receipt receipt = new Receipt(seller, customer);

        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 5);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        //System.out.println("ID = " + dao.add(receipt));
        //System.out.println("After" + receipt);
        //dao.delete(2);
        System.out.println(dao.getAll());
        System.out.println(dao.get(4));

    }

}
