/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author ASUS
 */
public class UserDao implements DaoInterface<User>{

    @Override
    public int add(User object) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            int id = -1;
            String insertUser = "INSERT INTO USER(NAME,TEL,PASSWORD)"
                    + "VALUES ('" + object.getName() 
                    + "','" + object.getTel() 
                    + "','" + object.getPassword()+"');";
            stmt.executeUpdate(insertUser);
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
            
            stmt.close();
            conn.commit();
            db.close();
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM USER;" );
            
            while(rs.next()){
                int id = rs.getInt("id");
                String  name = rs.getString("name");
                String  tel = rs.getString("tel");
                String password = rs.getString("password");
                User user = new User(id,name,tel,password);
                list.add(user);
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public User get(int id) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM USER WHERE ID = "
                                             + id +";");
            
            while(rs.next()){
                id = rs.getInt("id");
                String  name = rs.getString("name");
                String  tel = rs.getString("tel");
                String password = rs.getString("password");
                User user = new User(id,name,tel,password);
                return user;
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        int row =0;

        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            
            String deleteUser = "DELETE from USER where ID = '"
                                   + id +"';";
            row = stmt.executeUpdate(deleteUser);
            conn.commit();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return row;
    }

    @Override
    public int update(User object) {
        java.sql.Connection conn = null;
        Database db = Database.gatInstance();
        Statement stmt = null;
        conn = db.getConnection();
        int row =0;

        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("UPDATE USER set PASSWORD ="+object.getPassword()
                                    +" where ID="+object.getId()+"");
            conn.commit();
            stmt.close();
            db.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return row;
    }
    
    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User(-1,"Son","08999999","pass1234"));
        System.out.println("id : " + id);
        System.out.println(dao.get(id));
        User lastUser = dao.get(id);
        lastUser.setPassword("555555");
        int row = dao.update(lastUser);
        System.out.println(row);
        User updateUser = dao.get(id);
        System.out.println("update user:" + updateUser);
        dao.delete(id);
        User deleteProduct = dao.get(id);
        System.out.println("delete user: " + deleteProduct);
    }
    
}
