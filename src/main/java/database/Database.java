/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;
//Singleton pattren

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Database {

    private static Database instance = new Database();
    private Connection conn;

    private Database() {

    }

    public static Database gatInstance() {
        
            String dbName = "./db/store.db";
            try {
                if (instance.conn == null || instance.conn.isClosed()) {
                    Class.forName("org.sqlite.JDBC");
                    instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        return instance;
    }
    
    public static void close(){
        try {
            if(instance.conn != null && !instance.conn.isClosed()){
                instance.conn.close();
                   
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
         instance.conn = null;
    }
    
    public static Connection getConnection(){
        return instance.conn;
    }
}
